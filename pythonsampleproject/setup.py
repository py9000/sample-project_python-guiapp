import sys
from setuptools import setup

from pythonsampleproject.settings import *

assert sys.version_info >= MINIMUM_PYTHON_VERSION

setup(
    name="python-sample-project",
    version=VERSION,
    description="python-sample-project",
    author="Terminal Labs",
    author_email="solutions@terminallabs.com",
    license="mit",
    packages=["pythonsampleproject", "pythonsampleproject.tests"],
    zip_safe=False,
    include_package_data=True,
    install_requires=["setuptools", "pytest", "black", "pytest-cov", "flake8", "radon"],
    entry_points="""
      [console_scripts]
      pythonsampleproject=pythonsampleproject.__main__:main
  """,
)
